package com.tw.todoitems.service;

import com.tw.todoitems.model.Item;

import java.util.List;

public interface ToDoItems {
    List<Item> getAllItems();

    Item createItem(Item item);

    boolean updateItem(Item item);

    boolean deleteItem(int id);
}
