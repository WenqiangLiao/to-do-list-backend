package com.tw.todoitems;

import com.tw.todoitems.model.Item;
import com.tw.todoitems.model.ItemStatus;
import com.tw.todoitems.service.ToDoItems;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@RestController
//@RequestMapping("/items")
public class APP {

    @Autowired
    private ToDoItems toDoItems;

    public static void main(String[] args) {
        SpringApplication.run(APP.class, args);
    }

    @GetMapping("/items")
    @CrossOrigin(origins = "*")
    public List<Item> getAllItems() {
        return toDoItems.getAllItems();
    }

    @GetMapping("/say")
    public String say() {
        return "这里说了一句话。";
    }

    @PostMapping("/items")
    @CrossOrigin(origins = "*")
    public Item addItem(@RequestBody Item item) {
        // Need to be implemented
        return toDoItems.createItem(item);
    }

    @PutMapping("/items")
    @CrossOrigin(origins = "*")
    public boolean updateItem(@RequestBody Item item) {
        // Need to be implemented
        return toDoItems.updateItem(item);
//        return false;
    }

    @DeleteMapping("/items/{id}")
    @CrossOrigin(origins = "*")
    public void deleteItem(@PathVariable int id) {
        // Need to be implemented
        toDoItems.deleteItem(id);
    }
}
