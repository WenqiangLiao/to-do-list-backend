package com.tw.todoitems.db;

import com.tw.todoitems.model.Item;
import com.tw.todoitems.model.ItemStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ItemRepo {
    private dbConnector connector;

    public ItemRepo() {
        this.connector = new dbConnector();
    }

    public Item addItem(Item item) {
        String insertSql = String.format("insert into items (text, status) values ('%s', '%s')", item.getText(), item.getStatus());
        try (
            Connection connection = connector.createConnect();
            PreparedStatement insertPreparedStatement = connection.prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);)
        {
            insertPreparedStatement.execute();
            ResultSet resultSet = insertPreparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int keyindex = resultSet.getInt(1);

                String selectSql = String.format("select * from items where id='%d'", keyindex);
                Connection conn = connector.createConnect();
                Statement statement = conn.createStatement();
                statement.executeQuery(selectSql);
                ResultSet rs = statement.getResultSet();

                while (rs.next()) {
                    return new Item(rs.getInt("id"), rs.getString("text"), ItemStatus.valueOf(rs.getString("status")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Item> findItems() {
        String selectSql = "select id, text, status from items";
        List<Item> items = new ArrayList<Item>();

        try (Connection conn = connector.createConnect();
             Statement statement = conn.createStatement();)
          {
              statement.executeQuery(selectSql);
              ResultSet rs = statement.getResultSet();
              while (rs.next()) {
                  items.add(new Item(rs.getInt("id"), rs.getString("text"), ItemStatus.valueOf(rs.getString("status"))));
              }
          } catch (SQLException e) {
            e.printStackTrace();
        }
        return items;
    }

    public boolean updateItem(Item item) {
        String updateSql = String.format("update items set text='%s', status='%s' where id='%d'", item.getText(), item.getStatus(), item.getId());

        try (
                Connection conn = connector.createConnect();
                Statement statement = conn.createStatement();
                ){
            return (statement.executeUpdate(updateSql)) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteItem(int id) {
        String deleteSql = String.format("delete from items where id='%d'", id);

        try (
                Connection conn = connector.createConnect();
                Statement statement = conn.createStatement();
                ){
            return statement.executeUpdate(deleteSql) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
