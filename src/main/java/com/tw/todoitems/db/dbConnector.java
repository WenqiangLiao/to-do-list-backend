package com.tw.todoitems.db;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class dbConnector {
    private dbConfig config;

    public dbConnector() {
        Yaml ymal = new Yaml();
        InputStream inputStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream("db-config.yml");
        this.config = ymal.load(inputStream);
    }

    public dbConfig getConfig() {
        return config;
    }

    public Connection createConnect() throws SQLException {
//        Connection connection = null;
        try {
            Connection connection = DriverManager.getConnection(this.config.getDatabaseURL(), this.config.getUsername(), this.config.getPassword());
            return connection;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
