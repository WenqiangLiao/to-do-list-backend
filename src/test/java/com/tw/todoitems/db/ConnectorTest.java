package com.tw.todoitems.db;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

class ConnectorTest {

    private static dbConnector connector;

    @BeforeAll
    static void init() {
        connector = new dbConnector();
    }

    @Test
    void should_get_db_config_info() {
        dbConfig config = connector.getConfig();

        Assertions.assertNotNull(config);
        Assertions.assertEquals("jdbc:mysql://localhost:3306/student_manage_sys_liaowenqiang?useUnicode=yes&characterEncoding=UTF-8&serverTimezone=UTC", config.getDatabaseURL());
        Assertions.assertEquals("root", config.getUsername());
        Assertions.assertEquals("liaowenqiang", config.getPassword());
    }

    @Test
    void should_get_connect() throws SQLException {
        Connection connection = connector.createConnect();
        Assertions.assertNotNull(connection);
    }

}